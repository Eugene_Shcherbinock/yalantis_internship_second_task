It should be 3 tabs and Navigation drawer with test data (10 items). 

## Requirments: ##

* Put dimens and text sizes into res folder. 
* Use google code style !!!!!
* For first and second tab use recyclerview 
* On list item click, open first task screen
* For third tab use listview 
* When user scrolls list, floating action button should hide with animation 
* Use styles 
* Minimal SDK version = 16
* Only portrait orientation

![Screenshot_20160422-131914.png](https://bitbucket.org/repo/xknMbk/images/1740596513-Screenshot_20160422-131914.png)
![Screenshot_20160422-131855.png](https://bitbucket.org/repo/xknMbk/images/2889761996-Screenshot_20160422-131855.png)