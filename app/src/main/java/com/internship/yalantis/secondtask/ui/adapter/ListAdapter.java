package com.internship.yalantis.secondtask.ui.adapter;

import com.internship.yalantis.secondtask.interfaces.OnTaskItemClickListener;

public interface ListAdapter {

    void setOnTaskClickListener(OnTaskItemClickListener listener);

}
