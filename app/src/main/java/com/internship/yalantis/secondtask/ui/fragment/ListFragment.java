package com.internship.yalantis.secondtask.ui.fragment;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.internship.yalantis.secondtask.Constants;
import com.internship.yalantis.secondtask.R;
import com.internship.yalantis.secondtask.model.Task;
import com.internship.yalantis.secondtask.ui.adapter.TasksListAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListFragment extends BaseFragment {

    @Bind(R.id.list_view_tasks)
    ListView mListViewTasks;

    private TasksListAdapter mListViewAdapter;

    public static ListFragment newInstance(Task.Status taskStatus) {
        ListFragment fragment = new ListFragment();

        Bundle arguments = new Bundle();
        arguments.putInt(Constants.TASK_STATUS_ARG, taskStatus.ordinal());
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootFragmentView = inflater.inflate(R.layout.fragment_list, container, false);

        ButterKnife.bind(this, mRootFragmentView);
        ViewCompat.setNestedScrollingEnabled(mListViewTasks, true);

        mPresenter.getTasksByStatus(mTaskStatus);

        return mRootFragmentView;
    }

    @Override
    public void showTasks(List<Task> tasks) {
        mListViewAdapter = new TasksListAdapter(getContext(), tasks);
        mListViewAdapter.setOnTaskClickListener(this);
        mListViewTasks.setAdapter(mListViewAdapter);
    }
}
