package com.internship.yalantis.secondtask;

import android.app.Application;
import android.content.Context;

import com.internship.yalantis.secondtask.manager.TasksManager;

public class App extends Application {

    private static Context sContext;

    private static TasksManager sTasksManager;

    public static Context getContext() {
        return sContext;
    }

    public static TasksManager getTasksManager() {
        if (sTasksManager == null) {
            sTasksManager = new TasksManager();
            sTasksManager.initialize(sContext);
        }
        return sTasksManager;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
    }
}
