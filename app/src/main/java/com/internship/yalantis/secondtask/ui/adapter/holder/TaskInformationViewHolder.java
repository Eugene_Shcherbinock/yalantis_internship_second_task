package com.internship.yalantis.secondtask.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.internship.yalantis.secondtask.R;
import com.internship.yalantis.secondtask.interfaces.OnTaskItemClickListener;
import com.internship.yalantis.secondtask.model.Task;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TaskInformationViewHolder extends RecyclerView.ViewHolder {

    View mItemView;

    @Bind(R.id.image_view_category)
    ImageView mImageViewCategory;

    @Bind(R.id.text_view_category_name)
    TextView mTextViewCategory;

    @Bind(R.id.text_view_item_address)
    TextView mTextViewAddress;

    @Bind(R.id.text_view_item_date)
    TextView mTextViewDate;

    @Bind(R.id.text_view_item_days)
    TextView mTextViewDays;

    @Bind(R.id.text_view_likes_count)
    TextView mTextViewLikesCount;

    public TaskInformationViewHolder(View itemView) {
        super(itemView);
        mItemView = itemView;
        ButterKnife.bind(this, itemView);
    }

    public ImageView getImageViewCategory() {
        return mImageViewCategory;
    }

    public TextView getTextViewCategory() {
        return mTextViewCategory;
    }

    public TextView getTextViewAddress() {
        return mTextViewAddress;
    }

    public TextView getTextViewDate() {
        return mTextViewDate;
    }

    public TextView getTextViewDays() {
        return mTextViewDays;
    }

    public TextView getTextViewLikesCount() {
        return mTextViewLikesCount;
    }

    public void setOnItemClickListener(final Task task, final OnTaskItemClickListener listener) {
        mItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onTaskClick(task);
                }
            }
        });
    }
}
