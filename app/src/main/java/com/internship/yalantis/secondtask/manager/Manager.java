package com.internship.yalantis.secondtask.manager;

import android.content.Context;

public interface Manager {

    void initialize(Context context);

    void destroy();

}
