package com.internship.yalantis.secondtask;

public class Constants {

    public static final String APP_LOCALE = "uk";

    public static final String TASK_STATUS_ARG = "is_completed";

    public static final String TASK_ARG = "task";

    public static final int GENERATED_TASKS_COUNT = 10;

    public static final int MAXIMAL_LIKES_COUNT = 25;

    public static final int CATEGORIES_COUNT = 4;

    public static final int STATUSES_COUNT = 3;

}
