package com.internship.yalantis.secondtask.interfaces;

import com.internship.yalantis.secondtask.model.Task;

/**
 * Callback for start DetailActivity.class
 */
public interface OnShowTaskDetailsListener {

    void onShowDetails(Task task);

}
