package com.internship.yalantis.secondtask.ui.fragment;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.internship.yalantis.secondtask.Constants;
import com.internship.yalantis.secondtask.R;
import com.internship.yalantis.secondtask.model.Task;
import com.internship.yalantis.secondtask.ui.adapter.TasksRecyclerListAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class RecyclerFragment extends BaseFragment {

    @Bind(R.id.recycler_view_tasks)
    RecyclerView mRecyclerViewTasks;

    private TasksRecyclerListAdapter mRecyclerViewAdapter;

    public static RecyclerFragment newInstance(Task.Status taskStatus) {
        RecyclerFragment fragment = new RecyclerFragment();

        Bundle arguments = new Bundle();
        arguments.putInt(Constants.TASK_STATUS_ARG, taskStatus.ordinal());
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mRootFragmentView = inflater.inflate(R.layout.fragment_recycler, container, false);

        ButterKnife.bind(this, mRootFragmentView);

        RecyclerView.LayoutManager recyclerLayoutManager = new LinearLayoutManager(
                getContext(), LinearLayoutManager.VERTICAL, false
        );
        mRecyclerViewTasks.setLayoutManager(recyclerLayoutManager);

        mPresenter.getTasksByStatus(mTaskStatus);

        return mRootFragmentView;
    }

    @Override
    public void showTasks(List<Task> tasks) {
        mRecyclerViewAdapter = new TasksRecyclerListAdapter(getContext(), tasks);
        mRecyclerViewAdapter.setOnTaskClickListener(this);
        mRecyclerViewTasks.setAdapter(mRecyclerViewAdapter);
    }
}
