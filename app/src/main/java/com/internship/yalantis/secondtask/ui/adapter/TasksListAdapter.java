package com.internship.yalantis.secondtask.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.internship.yalantis.secondtask.R;
import com.internship.yalantis.secondtask.interfaces.OnTaskItemClickListener;
import com.internship.yalantis.secondtask.model.Task;
import com.internship.yalantis.secondtask.ui.adapter.holder.TaskInformationViewHolder;
import com.internship.yalantis.secondtask.util.DateUtils;
import com.internship.yalantis.secondtask.util.TaskUtils;

import java.util.List;

public class TasksListAdapter extends BaseAdapter implements ListAdapter {

    private Context mContext;
    private List<Task> mTasksList;
    private OnTaskItemClickListener mOnTaskClickListener;

    public TasksListAdapter(Context context, List<Task> tasks) {
        mContext = context;
        mTasksList = tasks;
    }

    @Override
    public void setOnTaskClickListener(OnTaskItemClickListener listener) {
        mOnTaskClickListener = listener;
    }

    @Override
    public int getCount() {
        return mTasksList.size();
    }

    @Override
    public Task getItem(int position) {
        return mTasksList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TaskInformationViewHolder viewHolder;
        View taskItemView = convertView;

        if (taskItemView == null) {
            taskItemView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.task_item_layout, parent, false);

            viewHolder = new TaskInformationViewHolder(taskItemView);
            taskItemView.setTag(viewHolder);
        } else {
            viewHolder = (TaskInformationViewHolder) taskItemView.getTag();
        }

        Task currentTask = mTasksList.get(position);

        viewHolder.getImageViewCategory().setImageDrawable(
                ContextCompat.getDrawable(mContext, TaskUtils.getCategoryIcon(currentTask.getCategory())));
        viewHolder.getTextViewCategory().setText(TaskUtils.getCategoryName(currentTask.getCategory()));

        viewHolder.getTextViewAddress().setText(currentTask.getAddress());
        viewHolder.getTextViewDate().setText(DateUtils.format(currentTask.getCreatedDate(),
                DateUtils.Format.TASK_CARD_FORMAT));

        int daysLeft = DateUtils.getDatesDifference(currentTask.getCreatedDate(), currentTask.getAssignedDate());
        viewHolder.getTextViewDays().setText(mContext.getResources().getQuantityString(R.plurals.str_days, daysLeft, daysLeft));
        viewHolder.getTextViewLikesCount().setText(String.valueOf(currentTask.getLikesCount()));

        viewHolder.setOnItemClickListener(currentTask, mOnTaskClickListener);

        return taskItemView;
    }
}
