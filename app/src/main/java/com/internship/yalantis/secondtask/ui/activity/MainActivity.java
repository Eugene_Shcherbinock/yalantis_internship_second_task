package com.internship.yalantis.secondtask.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.internship.yalantis.secondtask.Constants;
import com.internship.yalantis.secondtask.R;
import com.internship.yalantis.secondtask.interfaces.OnShowTaskDetailsListener;
import com.internship.yalantis.secondtask.model.Task;
import com.internship.yalantis.secondtask.ui.adapter.ViewPagerAdapter;
import com.internship.yalantis.secondtask.ui.fragment.ListFragment;
import com.internship.yalantis.secondtask.ui.fragment.RecyclerFragment;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnPageChange;

/**
 * MainActivity.class
 * <p/>
 * Show ViewPager with tasks lists
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnShowTaskDetailsListener {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.tab_layout)
    TabLayout mTabLayout;

    @Bind(R.id.text_view_navigation_footer)
    TextView mTextViewNavigationFooter;

    @Bind(R.id.view_pager_content)
    ViewPager mViewPagerContent;

    @Bind(R.id.float_button_add_item)
    FloatingActionButton mFloatButtonAddItem;

    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    private View.OnClickListener mOnNavigationIconClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        initializeNavigationView();
        initializeViewPager();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnPageChange(value = R.id.view_pager_content, callback = OnPageChange.Callback.PAGE_SELECTED)
    public void onPageSelected() {
        mFloatButtonAddItem.show();
    }

    @Override
    public void onShowDetails(Task task) {
        Intent detailActivityIntent = new Intent(this, DetailsActivity.class);
        detailActivityIntent.putExtra(Constants.TASK_ARG, task);
        startActivity(detailActivityIntent);
    }

    // TODO: getSupportActionBar() may produce NullPointerException
    @SuppressWarnings("ConstantConditions")
    private void initializeNavigationView() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawerToggle.setHomeAsUpIndicator(R.drawable.ic_menu);
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.setToolbarNavigationClickListener(mOnNavigationIconClickListener);
        drawerToggle.syncState();

        mDrawerLayout.addDrawerListener(drawerToggle);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        getSupportActionBar().setTitle(navigationView.getMenu().findItem(R.id.all_tasks_item).getTitle());
        mTextViewNavigationFooter.setMovementMethod(LinkMovementMethod.getInstance());
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initializeViewPager() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addNewPage(RecyclerFragment.newInstance(Task.Status.IN_PROCESS), getString(R.string.str_in_process_page_title));
        viewPagerAdapter.addNewPage(RecyclerFragment.newInstance(Task.Status.COMPLETED), getString(R.string.str_complete_page_title));
        viewPagerAdapter.addNewPage(ListFragment.newInstance(Task.Status.WAITING), getString(R.string.str_wait_page_title));

        mViewPagerContent.setAdapter(viewPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPagerContent);
    }
}
