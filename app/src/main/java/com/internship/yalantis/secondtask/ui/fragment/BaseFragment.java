package com.internship.yalantis.secondtask.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.internship.yalantis.secondtask.Constants;
import com.internship.yalantis.secondtask.contract.TasksContract;
import com.internship.yalantis.secondtask.interfaces.OnShowTaskDetailsListener;
import com.internship.yalantis.secondtask.interfaces.OnTaskItemClickListener;
import com.internship.yalantis.secondtask.model.Task;
import com.internship.yalantis.secondtask.presenter.TasksPresenter;

import java.util.List;

import butterknife.ButterKnife;

/**
 * BaseFragment.class
 * <p/>
 * Implements View for {@link TasksPresenter}
 * <p/>
 * Receive task status for getting tasks list and displaying it
 */
public class BaseFragment extends Fragment implements TasksContract.View, OnTaskItemClickListener {

    protected View mRootFragmentView;
    protected Task.Status mTaskStatus;

    protected TasksContract.Presenter mPresenter;

    private OnShowTaskDetailsListener mOnShowTaskDetailsListener;

    public BaseFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnShowTaskDetailsListener) {
            mOnShowTaskDetailsListener = (OnShowTaskDetailsListener) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            int statusOrdinal = getArguments().getInt(Constants.TASK_STATUS_ARG);
            mTaskStatus = Task.Status.values()[statusOrdinal];
        }
        mPresenter = new TasksPresenter(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        ButterKnife.unbind(this);
        mPresenter.detachView();
    }

    @Override
    public void showTasks(List<Task> tasks) {
    }

    @Override
    public void showTaskDetails(Task task) {
        if (mOnShowTaskDetailsListener != null) {
            // callback to MainActivity
            mOnShowTaskDetailsListener.onShowDetails(task);
        }
    }

    @Override
    public void onTaskClick(Task task) {
        mPresenter.selectTask(task);
    }
}
