package com.internship.yalantis.secondtask.presenter;

import com.internship.yalantis.secondtask.App;
import com.internship.yalantis.secondtask.contract.TasksContract;
import com.internship.yalantis.secondtask.manager.TasksManager;
import com.internship.yalantis.secondtask.model.Task;

import java.util.List;

/**
 * TaskPresenter.class
 * <p/>
 * Work with TaskManager and show data in View
 */
public class TasksPresenter implements TasksContract.Presenter {

    private TasksContract.View mView;
    private TasksManager mTasksManager;

    public TasksPresenter(TasksContract.View view) {
        attachView(view);
    }

    @Override
    public void getTasksByStatus(Task.Status taskStatus) {
        if (mView != null) {
            // get list of tasks with status
            List<Task> resultList = mTasksManager.getTasksByStatus(taskStatus);
            mView.showTasks(resultList);
        }
    }

    @Override
    public void attachView(TasksContract.View view) {
        mView = view;
        mTasksManager = App.getTasksManager();
    }

    @Override
    public void selectTask(Task task) {
        if (mView != null) {
            mView.showTaskDetails(task);
        }
    }

    @Override
    public void detachView() {
        mView = null;
        mTasksManager.destroy();
    }
}
