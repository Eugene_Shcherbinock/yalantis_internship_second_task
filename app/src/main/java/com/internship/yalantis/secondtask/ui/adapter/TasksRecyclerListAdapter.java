package com.internship.yalantis.secondtask.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.internship.yalantis.secondtask.R;
import com.internship.yalantis.secondtask.interfaces.OnTaskItemClickListener;
import com.internship.yalantis.secondtask.model.Task;
import com.internship.yalantis.secondtask.ui.adapter.holder.TaskInformationViewHolder;
import com.internship.yalantis.secondtask.util.DateUtils;
import com.internship.yalantis.secondtask.util.TaskUtils;

import java.util.List;

public class TasksRecyclerListAdapter extends RecyclerView.Adapter<TaskInformationViewHolder> implements ListAdapter {

    private Context mContext;
    private List<Task> mTasksList;
    private OnTaskItemClickListener mOnTaskClickListener;

    public TasksRecyclerListAdapter(Context context, List<Task> tasks) {
        mContext = context;
        mTasksList = tasks;
    }

    @Override
    public void setOnTaskClickListener(OnTaskItemClickListener listener) {
        mOnTaskClickListener = listener;
    }

    @Override
    public TaskInformationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View taskItemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.task_item_layout, parent, false);
        return new TaskInformationViewHolder(taskItemView);
    }

    @Override
    public void onBindViewHolder(TaskInformationViewHolder holder, int position) {
        Task currentTask = mTasksList.get(position);

        holder.getImageViewCategory().setImageDrawable(
                ContextCompat.getDrawable(mContext, TaskUtils.getCategoryIcon(currentTask.getCategory())));
        holder.getTextViewCategory().setText(TaskUtils.getCategoryName(currentTask.getCategory()));

        holder.getTextViewAddress().setText(currentTask.getAddress());
        holder.getTextViewDate().setText(DateUtils.format(currentTask.getCreatedDate(),
                DateUtils.Format.TASK_CARD_FORMAT));

        int daysLeft = DateUtils.getDatesDifference(currentTask.getCreatedDate(), currentTask.getAssignedDate());
        holder.getTextViewDays().setText(mContext.getResources().getQuantityString(R.plurals.str_days, daysLeft, daysLeft));
        holder.getTextViewLikesCount().setText(String.valueOf(currentTask.getLikesCount()));

        holder.setOnItemClickListener(currentTask, mOnTaskClickListener);
    }

    @Override
    public int getItemCount() {
        return mTasksList.size();
    }
}
