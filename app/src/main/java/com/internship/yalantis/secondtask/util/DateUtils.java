package com.internship.yalantis.secondtask.util;

import com.internship.yalantis.secondtask.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * DateUtils class
 * <p/>
 * Class for format dates
 */
public class DateUtils {

    private static final long MILLISECONDS_PER_DAY = 24 * 3600 * 1000;

    /**
     * Return current date
     *
     * @return current date
     */
    public static Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }

    /**
     * Returns the date before / after the {@param daysDifference} from the current date
     *
     * @param daysDifference count of days
     * @return date
     */
    public static Date getRandomDate(long daysDifference) {
        return new Date(System.currentTimeMillis() + (MILLISECONDS_PER_DAY * daysDifference));
    }

    /**
     * Return dates difference
     *
     * @param startDate start date
     * @param endDate end date
     * @return count of days
     */
    public static int getDatesDifference(Date startDate, Date endDate) {
        Date daysLeft = new Date(endDate.getTime() - startDate.getTime());
        return (int) (daysLeft.getTime() / MILLISECONDS_PER_DAY);
    }

    /**
     * Format date by default pattern
     *
     * @param date date that needs to be formated
     * @return string that holds date in pretty format
     */
    public static String format(Date date) {
        return new SimpleDateFormat(Format.NICE_FORMAT, new Locale(Constants.APP_LOCALE)).format(date);
    }

    /**
     * Format date by parameter pattern
     *
     * @param date   date that needs to be formated
     * @param format format pattern
     * @return string that holds date in pretty format
     */
    public static String format(Date date, String format) {
        return new SimpleDateFormat(format, new Locale(Constants.APP_LOCALE)).format(date);
    }

    /**
     * Formats holder
     */
    public abstract class Format {

        public static final String NICE_FORMAT = "dd MMMM y";

        public static final String TASK_CARD_FORMAT = "MMM d, yyyy";

    }
}
