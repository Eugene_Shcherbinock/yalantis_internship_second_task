package com.internship.yalantis.secondtask.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Task implements Serializable {

    private String mCode;
    private Category mCategory;
    private Status mStatus;
    private Date mCreatedDate;
    private Date mRegisteredDate;
    private Date mAssignedDate;
    private String mResponsibleName;
    private String mDescription;
    private String mAddress;
    private int mLikesCount;
    private List<String> mImagesUrlList;

    public Task() {
    }

    public Task(@NonNull Category category, @NonNull Status status,
                @NonNull Date created, @NonNull Date registered, @NonNull Date assigned,
                @NonNull String responsible, @NonNull String description, @NonNull String address,
                @NonNull int likesCount, @NonNull List<String> imagesUrlList) {

        mCategory = category;
        mStatus = status;
        mCreatedDate = created;
        mRegisteredDate = registered;
        mAssignedDate = assigned;
        mResponsibleName = responsible;
        mDescription = description;
        mAddress = address;
        mLikesCount = likesCount;
        mImagesUrlList = imagesUrlList;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public Category getCategory() {
        return mCategory;
    }

    public void setCategory(Category category) {
        mCategory = category;
    }

    public Status getStatus() {
        return mStatus;
    }

    public void setStatus(Status status) {
        mStatus = status;
    }

    public Date getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(Date createdDate) {
        mCreatedDate = createdDate;
    }

    public Date getRegisteredDate() {
        return mRegisteredDate;
    }

    public void setRegisteredDate(Date registeredDate) {
        mRegisteredDate = registeredDate;
    }

    public Date getAssignedDate() {
        return mAssignedDate;
    }

    public void setAssignedDate(Date assignedDate) {
        mAssignedDate = assignedDate;
    }

    public String getResponsibleName() {
        return mResponsibleName;
    }

    public void setResponsibleName(String responsibleName) {
        mResponsibleName = responsibleName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public int getLikesCount() {
        return mLikesCount;
    }

    public void setLikesCount(int likesCount) {
        mLikesCount = likesCount;
    }

    public List<String> getImagesUrlList() {
        return mImagesUrlList;
    }

    public void setImagesUrlList(List<String> imagesUrlList) {
        mImagesUrlList = imagesUrlList;
    }

    public enum Status {
        IN_PROCESS, COMPLETED, WAITING
    }

    public enum Category {
        CLEANING_TERRITORY, DEBT, DESTROY, OTHER
    }

}