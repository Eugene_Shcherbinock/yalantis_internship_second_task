package com.internship.yalantis.secondtask.contract;

import android.content.Context;

public interface BaseContract {

    interface View {

        Context getContext();

    }

    interface Presenter<V extends View> {

        void attachView(V view);

        void detachView();

    }
}
