package com.internship.yalantis.secondtask.interfaces;

import com.internship.yalantis.secondtask.model.Task;

/**
 * Callback for list(ListView, RecyclerView) item click
 */
public interface OnTaskItemClickListener {

    void onTaskClick(Task task);

}
