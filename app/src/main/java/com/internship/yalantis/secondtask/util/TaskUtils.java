package com.internship.yalantis.secondtask.util;

import com.internship.yalantis.secondtask.App;
import com.internship.yalantis.secondtask.R;
import com.internship.yalantis.secondtask.model.Task;

/**
 * TaskUtils.class
 */
public class TaskUtils {

    private static String[] sCategoriesArray = App.getContext()
            .getResources()
            .getStringArray(R.array.arr_category_values);

    private static String[] sStatusesArray = App.getContext()
            .getResources()
            .getStringArray(R.array.arr_status_values);

    /**
     * Return Drawable resource id for task category
     *
     * @param category task category
     * @return drawable id
     */
    public static int getCategoryIcon(Task.Category category) {
        switch (category) {
            case CLEANING_TERRITORY:
                return R.drawable.ic_cleaning_category;
            case DEBT:
                return R.drawable.ic_debt_category;
            case DESTROY:
                return R.drawable.ic_destroy_category;
            default:
                return R.drawable.ic_other_category;
        }
    }

    /**
     * Return category full name
     *
     * @param category task category
     * @return full name
     */
    public static String getCategoryName(Task.Category category) {
        return sCategoriesArray[category.ordinal()];
    }

    /**
     * Return status text
     *
     * @param status task status
     * @return status text
     */
    public static String getStatusText(Task.Status status) {
        return sStatusesArray[status.ordinal()];
    }

}
