package com.internship.yalantis.secondtask.manager;

import android.content.Context;
import android.content.res.Resources;

import com.internship.yalantis.secondtask.Constants;
import com.internship.yalantis.secondtask.R;
import com.internship.yalantis.secondtask.model.Task;
import com.internship.yalantis.secondtask.util.DateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * TaskManager.class
 * <p/>
 * Store data arrays from resources
 */
public class TasksManager implements Manager {

    private Context mContext;

    private String[] mCodesArray;
    private String[] mAddressesArray;
    private String[] mResponsiblesArray;
    private String[] mDescriptionsArray;
    private String[] mCategoriesArray;
    private String[] mImagesArray;

    private List<Task> mTasksList;

    public List<Task> getTasksByStatus(Task.Status status) {
        List<Task> resultList = new ArrayList<>();
        for (Task currentTask : mTasksList) {
            if (currentTask.getStatus() == status) {
                resultList.add(currentTask);
            }
        }
        return resultList;
    }

    @Override
    public void initialize(Context context) {
        mContext = context;

        Resources resources = mContext.getResources();

        // get all data from resources
        mCodesArray = resources.getStringArray(R.array.arr_tasks_codes);
        mAddressesArray = resources.getStringArray(R.array.arr_tasks_addresses);
        mResponsiblesArray = resources.getStringArray(R.array.str_tasks_responsibles);
        mDescriptionsArray = resources.getStringArray(R.array.arr_tasks_descriptions);
        mCategoriesArray = resources.getStringArray(R.array.arr_category_values);
        mImagesArray = resources.getStringArray(R.array.arr_tasks_images_urls);

        generateExampleTasks();
    }

    @Override
    public void destroy() {
//        if (mTasksList != null) {
//            mTasksList.clear();
//            mTasksList = null;
//        }
    }

    private void generateExampleTasks() {
        Random random = new Random();
        mTasksList = new ArrayList<>();

        for (int i = 0; i < Constants.GENERATED_TASKS_COUNT; i++) {
            Task currentTask = new Task();

            // get random task category
            currentTask.setCategory(Task.Category.values()[random.nextInt(Constants.CATEGORIES_COUNT)]);

            currentTask.setCode(mCodesArray[i]);
            currentTask.setAddress(mAddressesArray[i]);
            currentTask.setResponsibleName(mResponsiblesArray[0]);
            currentTask.setDescription(mDescriptionsArray[i]);

            // get random task status
            Task.Status taskStatus = Task.Status.values()[random.nextInt(Constants.STATUSES_COUNT)];
            currentTask.setStatus(taskStatus);
            switch (taskStatus) {

                // if task in process
                case IN_PROCESS:
                    // set created and registered dates to two days before today
                    currentTask.setCreatedDate(DateUtils.getRandomDate(-2));
                    currentTask.setRegisteredDate(DateUtils.getRandomDate(-2));

                    // set assigned date to three days after today
                    currentTask.setAssignedDate(DateUtils.getRandomDate(3));
                    break;

                // if task completed
                case COMPLETED:
                    // set created and registered dates to two days before today
                    currentTask.setCreatedDate(DateUtils.getRandomDate(-2));
                    currentTask.setRegisteredDate(DateUtils.getRandomDate(-2));

                    // set assigned date to yesterday
                    currentTask.setAssignedDate(DateUtils.getRandomDate(-1));
                    break;

                // if task wait
                default:
                    // set created and registered dates to two days before today
                    currentTask.setCreatedDate(DateUtils.getCurrentDate());
                    currentTask.setRegisteredDate(DateUtils.getCurrentDate());

                    // set assigned date to five days after today
                    currentTask.setAssignedDate(DateUtils.getRandomDate(4));
            }

            currentTask.setLikesCount(random.nextInt(Constants.MAXIMAL_LIKES_COUNT));
            currentTask.setImagesUrlList(Arrays.asList(mImagesArray));

            mTasksList.add(currentTask);
        }
    }
}
