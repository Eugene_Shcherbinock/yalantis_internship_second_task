package com.internship.yalantis.secondtask.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.internship.yalantis.secondtask.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * TaskImagesViewHolder class
 * <p/>
 * Class holds ImageView for representing image of task to user
 */
public class TaskImagesViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.image_view_task)
    ImageView mImageView;

    public TaskImagesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ImageView getImageView() {
        return mImageView;
    }
}
