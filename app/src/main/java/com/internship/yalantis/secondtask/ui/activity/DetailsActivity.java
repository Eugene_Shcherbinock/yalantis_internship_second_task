package com.internship.yalantis.secondtask.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.internship.yalantis.secondtask.Constants;
import com.internship.yalantis.secondtask.R;
import com.internship.yalantis.secondtask.model.Task;
import com.internship.yalantis.secondtask.ui.adapter.ImagesAdapter;
import com.internship.yalantis.secondtask.util.DateUtils;
import com.internship.yalantis.secondtask.util.TaskUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * DetailsActivity.class
 * <p/>
 * Show full task information
 */
public class DetailsActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.text_view_title)
    TextView mTextViewTaskTitle;

    @Bind(R.id.text_view_status)
    TextView mTextViewTaskStatus;

    @Bind(R.id.text_view_created)
    TextView mTextViewTaskCreated;

    @Bind(R.id.text_view_registered)
    TextView mTextViewTaskRegistered;

    @Bind(R.id.text_view_assigned)
    TextView mTextViewTaskAssigned;

    @Bind(R.id.text_view_responsible)
    TextView mTextViewTaskResponsible;

    @Bind(R.id.text_view_description)
    TextView mTextViewTaskDescription;

    @Bind(R.id.recycler_view_images)
    RecyclerView mRecyclerViewTaskImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);

        ButterKnife.bind(this);
        initializeToolbar();

        receiveArguments();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void receiveArguments() {
        Intent receiveIntent = getIntent();
        if (receiveIntent.getExtras() != null) {
            Task showingTask = (Task) receiveIntent.getExtras().getSerializable(Constants.TASK_ARG);
            showTask(showingTask);
        }
    }

    // TODO: getSupportActionBar() may produce NullPointerException
    @SuppressWarnings("ConstantConditions")
    private void showTask(Task task) {
        getSupportActionBar().setTitle(task.getCode());

        mTextViewTaskTitle.setText(TaskUtils.getCategoryName(task.getCategory()));
        mTextViewTaskStatus.setText(TaskUtils.getStatusText(task.getStatus()));
        mTextViewTaskCreated.setText(DateUtils.format(task.getCreatedDate()));
        mTextViewTaskRegistered.setText(DateUtils.format(task.getRegisteredDate()));
        mTextViewTaskAssigned.setText(DateUtils.format(task.getAssignedDate()));
        mTextViewTaskResponsible.setText(task.getResponsibleName());
        mTextViewTaskDescription.setText(task.getDescription());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL, false
        );
        mRecyclerViewTaskImages.setLayoutManager(layoutManager);
        mRecyclerViewTaskImages.setAdapter(new ImagesAdapter(this, task.getImagesUrlList()));
    }

    // TODO: getSupportActionBar() may produce NullPointerException
    @SuppressWarnings("ConstantConditions")
    private void initializeToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
