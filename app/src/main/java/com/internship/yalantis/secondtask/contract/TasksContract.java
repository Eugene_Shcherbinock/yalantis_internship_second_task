package com.internship.yalantis.secondtask.contract;

import com.internship.yalantis.secondtask.model.Task;

import java.util.List;

public interface TasksContract {

    interface View extends BaseContract.View {

        void showTasks(List<Task> tasks);

        void showTaskDetails(Task task);

    }

    interface Presenter extends BaseContract.Presenter<View> {

        void getTasksByStatus(Task.Status taskStatus);

        void selectTask(Task task);

    }

}
